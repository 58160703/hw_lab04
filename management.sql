CREATE VIEW management AS
SELECT Dnumber,Dname,Fname,Lname
FROM Department JOIN  Manager
WHERE  Department.DNumber = Manager.MNumber
ORDER BY Department.ManagerID;
