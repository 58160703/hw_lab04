CREATE VIEW dept AS
SELECT DName, LocationName
FROM Department  JOIN Dept_Locations
WHERE Dept_Locations.Lnumber = Department.Dnumber
ORDER BY Department.ManagerID;
