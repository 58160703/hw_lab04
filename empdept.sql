CREATE VIEW empdept AS 
SELECT e.Fname,e.Lname,e.Salary,d.Dname,l.LocationName
FROM
     Employee e
	INNER JOIN
     Department d on e.EmpID = d.Dnumber
	INNER JOIN
     Dept_Locations l on l.Lnumber=d.Location_id
ORDER BY d.Dnumber;
